import 'dart:math';
import 'package:flutter/material.dart';
class StepperScreen extends StatefulWidget {
  StepperScreen({Key key}) : super(key: key);

  @override
  _StepperScreenState createState() => _StepperScreenState();
}

class _StepperScreenState extends State<StepperScreen> {
  
  static List<GlobalKey<FormState>> key = [
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
  ];

  int currentStep = 0;
  bool isActiveStep = true;
  
  //data
  String name = '';
  String phone = '';
  String email = '';
  String age = '';

  @override
  void initState() { 
    super.initState();
    
  }

  
  @override
  Widget build(BuildContext context) {

    List<Step> steps = [
      Step(
        state: currentStep == 0 ? 
          //user is editing that field
          StepState.editing : 
        currentStep > 0 ? 
          /*
            If user done field and pass to another the state is complete, 
            if isn't will show index number of step 
          */
          StepState.complete : StepState.indexed,
        
        title: Text('Add your name'),
        content: Form(
          key: key[0],
          child: TextFormField(
            autovalidateMode: AutovalidateMode.always,
            validator: (String text){
              if(text.isEmpty){
                return 'esta vacio';
              }
              else{
                return null;
              }
            },
            onSaved: (String val) {
              setState(() {
                name = val;
              });
              print(name);
            },
          ),
        ),
        isActive: true,
        
      ),
      Step(
        state: currentStep == 1 ? 
        //user is editing that field
          StepState.editing : 
        currentStep > 0 ? 
          /*
            If user done field and pass to another the state is complete, 
            if isn't will show index number of step 
          */
          StepState.complete : StepState.indexed,
        title: Text('Add your username'),
        content: Form(
          autovalidateMode: AutovalidateMode.always,
          key: key[1],
          child: TextFormField(
          ),
        ),
        isActive: true
      )
    ];

    return Scaffold(
       body: Stepper(
          steps: steps,
          currentStep: currentStep,
          onStepContinue: (){
            if(currentStep < steps.length - 1){
              if(key[currentStep].currentState.validate()){
                setState((){ 
                  currentStep = currentStep + 1;
                });
              }
            }
            else{
              print('este es el ultimo paso');
            }
          },
          onStepCancel: (){
            setState(() {
              if(currentStep > 0){
                currentStep = currentStep - 1;
              }
            });
          },
       ),
    );
  }
}